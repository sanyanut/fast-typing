export const createRoom = (socket) => {
  const roomName = prompt("Enter the name of your room", "Default name");
  socket.emit("CREATE_ROOM", roomName);
};

export const joinRoom = (socket, id) => {
  socket.emit("JOIN_ROOM", id);
};

export const createElement = ({
  tagName,
  className,
  text,
  attributes = {},
  parentNode,
}) => {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(" ").filter(Boolean);
    element.classList.add(...classNames);
  }
  if (text) {
    element.innerText = text;
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );
  if (parentNode) {
    parentNode.append(element);
  }
  return element;
};

export const createRoomElement = (room, root, socket) => {
  const { name, count, id, available } = room;
  if (available) {
    const roomElement = createElement({
      tagName: "div",
      className: "room-item",
      attributes: { id: id },
      parentNode: root,
    });
    const countUsers = count === 1 ? "1 user" : `${count} users`;
    createElement({
      tagName: "span",
      className: "room-item-connected",
      text: `${countUsers} connected`,
      parentNode: roomElement,
    });
    createElement({
      tagName: "h3",
      className: "room-item-name",
      text: name,
      parentNode: roomElement,
    });
    const roomButoon = createElement({
      tagName: "button",
      className: "room-item-button",
      text: "Join",
      parentNode: roomElement,
    });
    roomButoon.addEventListener("click", joinRoom.bind(null, socket, id));
  }
};

export const switchVisibility = ({ elementToShow, elementToHide }) => {
  elementToHide.classList.add("display-none");
  elementToShow.classList.remove("display-none");
};

export const createModal = (bodyElement) => {
  const layer = createElement({
    tagName: "div",
    className: "modal-layer",
    parentNode: root,
  });
  const modalContainer = createElement({
    tagName: "div",
    className: "modal-root",
    parentNode: layer,
  });
  const headerElement = createElement({
    tagName: "div",
    className: "modal-header",
    parentNode: modalContainer,
  });
  createElement({
    tagName: "span",
    text: "Rusults of the game",
    parentNode: headerElement,
  });
  const closeButton = createElement({
    tagName: "div",
    className: "close-btn",
    text: "×",
    parentNode: headerElement,
  });

  const close = () => {
    const modal = document.querySelector(".modal-layer");
    modal?.remove();
  };

  closeButton.addEventListener("click", close);
  modalContainer.append(bodyElement);
};

export const displayUsers = (users, activePlayer) => {
  const gameAside = document.getElementById("game-aside");

  const userContainer = createElement({
    tagName: "div",
    attributes: {
      id: "user-container",
    },
    parentNode: gameAside,
  });

  users.forEach((user, index) => {
    const { id, username, ready } = user;

    const userListItem = createElement({
      tagName: "div",
      className: "user-item",
      attributes: {
        id: id,
      },
      parentNode: userContainer,
    });

    const userIndicator = createElement({
      tagName: "span",
      className: "user-indicator",
      parentNode: userListItem,
    });

    ready
      ? userIndicator.classList.add("ready")
      : userIndicator.classList.add("not-ready");

    createElement({
      tagName: "span",
      className: "user-name",
      text: username,
      parentNode: userListItem,
    });

    createElement({
      tagName: "span",
      className: "user-id",
      text: `#${index + 1}`,
      parentNode: userListItem,
    });

    if (username === activePlayer) {
      createElement({
        tagName: "span",
        className: "user-active",
        text: "(you)",
        parentNode: userListItem,
      });
    }

    createElement({
      tagName: "progress",
      attributes: {
        id: "progress",
        max: "100",
        value: "0",
      },
      parentNode: userListItem,
    });
  });
};

export const createRating = (rating) => {
  const ratingElement = createElement({ tagName: "ul", className: "rating" });
  rating.map(({ username, timing, progress }) => {
    const ratingItem = createElement({
      tagName: "li",
      className: "rating-item",
      parentNode: ratingElement,
    });
    createElement({ tagName: "span", text: username, parentNode: ratingItem });
    progress &&
      createElement({
        tagName: "span",
        text: `${progress}%`,
        parentNode: ratingItem,
      });
    timing &&
      createElement({
        tagName: "span",
        text: `${timing / 1000} sec`,
        parentNode: ratingItem,
      });
  });
  return ratingElement;
};
